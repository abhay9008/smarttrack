package com.example.abhay.mapdemo.networking;

import com.example.abhay.mapdemo.model.Location;
import com.example.abhay.mapdemo.model.VehicleLocation;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by nitin on 6/10/17.
 */

public interface ApiInterface {
    @GET("VTS_APIs/login")
    Call<Location> getLocations(@Header("account_id") String accountId, @Header("user_id") String userId, @Header("password") String password);

    @GET("VTS_APIs/eventdetails")
    Call<List<VehicleLocation>> getVehicleLocation(@Header("account_id") String accountId, @Header("user_id") String userId, @Header("password") String password,
                                                   @Header("device_id") String deviceId, @Header("from_time") String fromTime, @Header("to_time") String toTime);
}
