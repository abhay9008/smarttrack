package com.example.abhay.mapdemo.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.adapter.LiveAdapter;
import com.example.abhay.mapdemo.customViews.RecyclerItemClickListener;
import com.example.abhay.mapdemo.customViews.SimpleDividerItemDecoration;
import com.example.abhay.mapdemo.model.Location;
import com.example.abhay.mapdemo.model.VehicleLocation;
import com.example.abhay.mapdemo.networking.ApiClient;
import com.example.abhay.mapdemo.networking.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveTracking extends AppCompatActivity implements OnMapReadyCallback {

    String mAccountId ="";
    String mUserId ="";
    String mPasssword ="";
    String mDeviceId ="";
    String tempFromDate = "";
    String tempToDate = "";
    String mFromTime ="2017-10-24 09:50:00";
    String mToTime ="2017-10-24 10:20:00";
    private GoogleMap mMap;
    private Location objLocation;
    List<VehicleLocation> listOfLocation = new ArrayList<>();
    private ProgressDialog dialog;
    private RecyclerView rlLiveVehicles;
    private LiveAdapter adapter;

    private CountDownTimer countDownTimer;
    private final long startTime =60 * 1000;
    private final long interval = 60 * 1000;

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    static final int DATE_DIALOG_ID = 998;
    static final int TIME_DIALOG_ID = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_tracking);
        rlLiveVehicles = (RecyclerView) findViewById(R.id.live_tracking_vehicle_recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Vehicle Tracking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                finish();
            }
        });
        dialog = new ProgressDialog(this);
        dialog.setTitle("Please wait");
        dialog.setMessage("Getting vehicle location");
        countDownTimer = new MyCountDownTimer(startTime, interval);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mAccountId = getIntent().getStringExtra("account_id");
        mUserId = getIntent().getStringExtra("user_id");
        mPasssword = getIntent().getStringExtra("password");
        mDeviceId = getIntent().getStringExtra("device_id");
        objLocation = getIntent().getExtras().getParcelable("location");
        mFromTime = getYesterdaysDate();
        mToTime = getTodaysDate();
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        setAdapter();
        getVehicleLocationData();
    }

    private void setAdapter(){
        adapter = new LiveAdapter(this, objLocation.getDeviceBean());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        rlLiveVehicles.setLayoutManager(mLayoutManager);
        rlLiveVehicles.setItemAnimator(new DefaultItemAnimator());
        rlLiveVehicles.addItemDecoration(new SimpleDividerItemDecoration(this));
        rlLiveVehicles.setAdapter(adapter);
        rlLiveVehicles.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mDeviceId = objLocation.getDeviceBean().get(position).getDeviceID();
                getVehicleLocationData();
            }
        }));
    }

    public static String getTodaysDate(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");
        String strDate = sdf.format(cal.getTime());
        strDate = strDate.concat(" 00:00:00");
        return strDate;
    }

    public static String getYesterdaysDate(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");
        String strDate = sdf.format(cal.getTime());
        strDate = strDate.concat(" 00:00:00");
        return strDate;
    }

    public static String getNextDay(String currentDay){
        String dt = currentDay;  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String output = sdf1.format(c.getTime());
        return output;
    }
    
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void getVehicleLocationData(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        dialog.show();

        Call<List<VehicleLocation>> call = apiService.getVehicleLocation(mAccountId, mUserId, mPasssword, mDeviceId, mFromTime, mToTime);
        call.enqueue(new Callback<List<VehicleLocation>>() {
            @Override
            public void onResponse(Call<List<VehicleLocation>>call, Response<List<VehicleLocation>> response) {
                listOfLocation = response.body();
                dialog.dismiss();
                try {
                    if (listOfLocation.size() > 0) {
                        ArrayList<LatLng> points = new ArrayList<>();
                        PolylineOptions lineOptions =  new PolylineOptions();

                        for (int i = 0; i < listOfLocation.size(); i++) {
                            if (listOfLocation.get(i).getLatitude() != null && !listOfLocation.get(i).getLatitude().isEmpty() && listOfLocation.get(i).getLongitude() != null && !listOfLocation.get(i).getLongitude().isEmpty()) {
                                double lat = Double.parseDouble(listOfLocation.get(i).getLatitude());
                                double lng = Double.parseDouble(listOfLocation.get(i).getLongitude());
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        lineOptions.addAll(points);
                        lineOptions.width(10);
                        lineOptions.color(Color.RED);

                        LatLngBounds.Builder bc = new LatLngBounds.Builder();

                        for (LatLng item : points) {
                            bc.include(item);
                        }

                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));

                        if (lineOptions != null) {
                            mMap.addPolyline(lineOptions);
                        } else {
                            Log.d("onPostExecute", "without Polylines drawn");
                        }

                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(listOfLocation.get(0).getLatitude()),
                                Double.parseDouble(listOfLocation.get(0).getLongitude()))));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(listOfLocation.get(listOfLocation.size() - 1).getLatitude()),
                                Double.parseDouble(listOfLocation.get(listOfLocation.size() - 1).getLongitude()))));

                    }
                    else
                        mMap.clear();
                    countDownTimer.start();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<VehicleLocation>>call, Throwable t) {
                // Log error here since request failed
                Log.d("onFailure", t.getMessage());
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }


    @Override
    protected void onStop() {
        countDownTimer.cancel();
        super.onStop();
    }

    @Override
    protected void onPause() {
        countDownTimer.cancel();
        super.onPause();
    }

    @Override
    protected void onResume() {
        countDownTimer.start();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.live_tracking_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_calendar) {
            showDialog(DATE_DIALOG_ID);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month,day);
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this,
                        timePickerListener, hour, minute,false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            month++;
            tempFromDate = selectedYear+"-"+month+"-"+selectedDay;
            tempToDate = getNextDay(tempFromDate);
            showDialog(TIME_DIALOG_ID);
        }
    };

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    tempFromDate = tempFromDate.concat(" "+selectedHour+":"+selectedMinute+":"+"00");
                    mFromTime = tempFromDate;
                    tempToDate = tempToDate.concat(" "+selectedHour+":"+selectedMinute+":"+"00");
                    mToTime = tempToDate;
                    getVehicleLocationData();
                }
            };

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            getVehicleLocationData();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}
