package com.example.abhay.mapdemo.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.abhay.mapdemo.R;

import static android.R.attr.animation;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener{

    Animation bounceAnim;
    ImageView imgMarker, imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initUI();
        setBounceAnim();
        showLogo();
    }

    private void initUI(){
        imgMarker = (ImageView) findViewById(R.id.imageView);
        imgLogo = (ImageView) findViewById(R.id.image_view_launcher_logo);
    }

    private void setBounceAnim(){
        bounceAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        //bounceAnim.setAnimationListener(this);
        imgMarker.startAnimation(bounceAnim);
    }

    private void showLogo(){
        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        fadeIn.setAnimationListener(SplashActivity.this);
        imgLogo.startAnimation(fadeIn);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        imgLogo.setVisibility(View.VISIBLE);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
