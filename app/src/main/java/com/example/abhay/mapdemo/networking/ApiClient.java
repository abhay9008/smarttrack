package com.example.abhay.mapdemo.networking;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nitin on 6/10/17.
 */

public class ApiClient {
    //public static final String BASE_URL = "http://202.153.42.247:8080/";
    public static final String BASE_URL = "http://smartngy.com:8080//";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
