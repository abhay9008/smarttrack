package com.example.abhay.mapdemo.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.customViews.BottomNavigationViewHelper;
import com.example.abhay.mapdemo.fragments.DashboardFragment;
import com.example.abhay.mapdemo.fragments.GroupFragment;
import com.example.abhay.mapdemo.fragments.LiveFragment;
import com.example.abhay.mapdemo.model.Location;

public class Home extends AppCompatActivity {

    private Location objLocation;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        fab = (FloatingActionButton) findViewById(R.id.live_tracking_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, LiveTracking.class);
                i.putExtra("location", objLocation);
                i.putExtra("account_id",objLocation.getmAccountId());
                i.putExtra("user_id",objLocation.getmUserId());
                i.putExtra("password",objLocation.getmPassword());
                i.putExtra("device_id",objLocation.getDeviceBean().get(0).getDeviceID());
                startActivity(i);
            }
        });
        objLocation = getIntent().getExtras().getParcelable("location");
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                selectedFragment = DashboardFragment.newInstance();
                                break;
                            case R.id.action_vehicle:
                                selectedFragment = LiveFragment.newInstance();
                                break;
                            case R.id.action_group:
                                selectedFragment = GroupFragment.newInstance();
                                break;
                            /*case R.id.action_live:
                                selectedFragment = DashboardFragment.newInstance();
                                break;*/
                        }
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("location", objLocation);
                        selectedFragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        bottomNavigationView.setSelectedItemId(R.id.action_group);

        //Manually displaying the first fragment - one time only
        Fragment selectedFragment = GroupFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putParcelable("location", objLocation);
        selectedFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }
}
