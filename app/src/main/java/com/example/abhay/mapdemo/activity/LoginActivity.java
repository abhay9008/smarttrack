package com.example.abhay.mapdemo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.abhay.mapdemo.networking.ApiClient;
import com.example.abhay.mapdemo.networking.ApiInterface;
import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.model.Location;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nitin on 19/9/17.
 */

public class LoginActivity extends AppCompatActivity{

    private TextInputLayout tilUserId, tilPassword, tilAccountId;
    private EditText edtUserId, edtPassword, edtAccountId;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edtUserId = (EditText) findViewById(R.id.edit_text_launcher_user_id);
        edtPassword = (EditText) findViewById(R.id.edit_text_launcher_password);
        edtAccountId = (EditText) findViewById(R.id.edit_text_launcher_account_id);
        tilUserId = (TextInputLayout) findViewById(R.id.input_layout_launcher_user_id);
        dialog = new ProgressDialog(this);
        dialog.setTitle("Please wait");
        dialog.setMessage("Verifying login credentials");
    }

    /**
     * on click of login button
     * @param v
     */
    public void signIn(View v){
        changeErrorStatus(false, null, edtUserId);
        changeErrorStatus(false, null, edtAccountId);
        changeErrorStatus(false, null, edtPassword);
//        if( edtUserId.getText().toString().isEmpty() )
//            changeErrorStatus(true, "Please enter user Id", edtUserId);
        if( edtAccountId.getText().toString().isEmpty() )
            changeErrorStatus(true, "Please enter account Id", edtAccountId);
        else if( edtPassword.getText().toString().isEmpty() )
            changeErrorStatus(true, "Please enter password", edtPassword);
        else makeLoginCall();

    }

    /**
     * mwethod to make login api call
     */
    private void makeLoginCall() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        dialog.show();

        Call<Location> call = apiService.getLocations(edtAccountId.getText().toString(), edtUserId.getText().toString(), edtPassword.getText().toString());
        call.enqueue(new Callback<Location>() {
             @Override
            public void onResponse(Call<Location>call, Response<Location> response) {
                dialog.dismiss();
                if( response.body().getLoginStatus().equals("Success")){
                    response.body().setmUserId(edtUserId.getText().toString());
                    response.body().setmAccountId(edtAccountId.getText().toString());
                    response.body().setmPassword(edtPassword.getText().toString());
                    Intent i = new Intent(LoginActivity.this, Home.class);
                    i.putExtra("location", response.body());
                    startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }else
                    Toast.makeText(getApplicationContext(), "Invalid credentials, please try again?", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Location>call, Throwable t) {
                // Log error here since request failed
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * method to set error on edit text
     * @param isError
     * @param message
     * @param edtTemp
     */
    private void changeErrorStatus(boolean isError, String message, EditText edtTemp){
        if(isError) {
            edtTemp.setError(message);
            edtTemp.requestFocus();
        }
        else edtTemp.setError(null);
    }

}
