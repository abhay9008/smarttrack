package com.example.abhay.mapdemo.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.activity.VehicleTracking;
import com.example.abhay.mapdemo.adapter.VehicleAdapter;
import com.example.abhay.mapdemo.customViews.RecyclerItemClickListener;
import com.example.abhay.mapdemo.customViews.SimpleDividerItemDecoration;
import com.example.abhay.mapdemo.model.Location;

import static android.support.v7.recyclerview.R.attr.layoutManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link LiveFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LiveFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private Location objLocation;
    private View view;
    private RecyclerView rlLiveVehicles;
    private VehicleAdapter adapter;


    public LiveFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment LiveFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LiveFragment newInstance() {
        LiveFragment fragment = new LiveFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        objLocation = getArguments().getParcelable("location");
        view = inflater.inflate(R.layout.fragment_live, container, false);
        rlLiveVehicles = (RecyclerView) view.findViewById(R.id.recycler_view);
        setAdapter();
        return view;
    }

    private void setAdapter(){
        adapter = new VehicleAdapter(getActivity(), objLocation.getDeviceBean());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rlLiveVehicles.setLayoutManager(mLayoutManager);
        rlLiveVehicles.setItemAnimator(new DefaultItemAnimator());
        rlLiveVehicles.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rlLiveVehicles.setAdapter(adapter);
        rlLiveVehicles.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(getActivity(), VehicleTracking.class);
                i.putExtra("account_id",objLocation.getmAccountId());
                i.putExtra("user_id",objLocation.getmUserId());
                i.putExtra("password",objLocation.getmPassword());
                i.putExtra("device_id",objLocation.getDeviceBean().get(position).getDeviceID());
                startActivity(i);
            }
        }));
    }

}
