package com.example.abhay.mapdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.model.DeviceBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by nitin on 25/10/17.
 */

public class VehicleAdapter extends  RecyclerView.Adapter<VehicleAdapter.LiveViewHolder>{

    private List<DeviceBean> listOfVehicles = new ArrayList<>();
    private Context cntxt;

    public VehicleAdapter(Context cntxt, List<DeviceBean> listOfVehicles){
        this.cntxt = cntxt;
        this.listOfVehicles = listOfVehicles;
    }

    @Override
    public VehicleAdapter.LiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_live, parent, false);

        return new VehicleAdapter.LiveViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VehicleAdapter.LiveViewHolder holder, int position) {
        DeviceBean objLocation = listOfVehicles.get(position);
        holder.lisencePlate.setText("License Plate : " +getMessage( objLocation.getLicensePlate() ));
        holder.lastFuelLevel.setText("Last fuel level : " +getMessage( objLocation.getFuelLevel() ));
        holder.description.setText("Description : " +getMessage( "" ));
        holder.timeStamp.setText("Time : " +getFormatedDate( objLocation.getLastCommunication() ));
        holder.location.setText("Location : " +getMessage( objLocation.getLatitude() ) +" / " +getMessage( objLocation.getLongitude() ));
    }

    @Override
    public int getItemCount() {
        return listOfVehicles.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class LiveViewHolder extends RecyclerView.ViewHolder {
        public TextView lisencePlate, lastFuelLevel, description, timeStamp, location;

        public LiveViewHolder(View view) {
            super(view);
            lisencePlate = (TextView) view.findViewById(R.id.adapter_live_lisence_plate);
            lastFuelLevel = (TextView) view.findViewById(R.id.adapter_live_last_fuel_level);
            description = (TextView) view.findViewById(R.id.adapter_live_description);
            timeStamp = (TextView) view.findViewById(R.id.adapter_live_time);
            location = (TextView) view.findViewById(R.id.adapter_live_location);
        }
    }

    private String getMessage(String message){
        try {
            if (message.isEmpty() || message.equals(null) || message.equals(" ")) {
                return "Not Available";
            } else
                return message;
        }catch (NullPointerException ne){
            ne.printStackTrace();
            return "Not Available";
        }
    }

    private String getFormatedDate(String timeStamp){
        /*if (timeStamp != null && timeStamp != "") {
            long itemLong = (long) (Integer.parseInt(timeStamp) / 1000);
            java.util.Date d = new java.util.Date(itemLong * 1000L);
            String itemDateStr = new SimpleDateFormat("dd-MMM HH:mm").format(d);
            return itemDateStr;
        }else return "Not Available";*/
        return timeStamp;
    }

}
