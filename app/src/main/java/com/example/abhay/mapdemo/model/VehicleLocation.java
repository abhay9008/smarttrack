package com.example.abhay.mapdemo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleLocation {

    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("speedKPH")
    @Expose
    private String speedKPH;
    @SerializedName("fuelLevel")
    @Expose
    private String fuelLevel;
    @SerializedName("digitalInput0")
    @Expose
    private String digitalInput0;
    @SerializedName("licensePlate")
    @Expose
    private String licensePlate;
    @SerializedName("indexCount")
    @Expose
    private String indexCount;
    @SerializedName("description")
    @Expose
    private String description;

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSpeedKPH() {
        return speedKPH;
    }

    public void setSpeedKPH(String speedKPH) {
        this.speedKPH = speedKPH;
    }

    public String getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(String fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public String getDigitalInput0() {
        return digitalInput0;
    }

    public void setDigitalInput0(String digitalInput0) {
        this.digitalInput0 = digitalInput0;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getIndexCount() {
        return indexCount;
    }

    public void setIndexCount(String indexCount) {
        this.indexCount = indexCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}