
package com.example.abhay.mapdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceBean implements Parcelable {

    @SerializedName("deviceID")
    @Expose
    private String deviceID;
    @SerializedName("vehicleID")
    @Expose
    private String vehicleID;
    @SerializedName("license_plate")
    @Expose
    private String licensePlate;
    @SerializedName("fuel_capacity")
    @Expose
    private String fuelCapacity;
    @SerializedName("speed_limit")
    @Expose
    private String speedLimit;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("last_communication")
    @Expose
    private String lastCommunication;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("signal")
    @Expose
    private String signal;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("heading")
    @Expose
    private String heading;
    @SerializedName("ignition")
    @Expose
    private String ignition;
    @SerializedName("fuel_level")
    @Expose
    private String fuelLevel;
    @SerializedName("satellite")
    @Expose
    private String satellite;

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(String fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public String getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(String speedLimit) {
        this.speedLimit = speedLimit;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getLastCommunication() {
        return lastCommunication;
    }

    public void setLastCommunication(String lastCommunication) {
        this.lastCommunication = lastCommunication;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getIgnition() {
        return ignition;
    }

    public void setIgnition(String ignition) {
        this.ignition = ignition;
    }

    public String getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(String fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public String getSatellite() {
        return satellite;
    }

    public void setSatellite(String satellite) {
        this.satellite = satellite;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.deviceID);
        dest.writeString(this.vehicleID);
        dest.writeString(this.licensePlate);
        dest.writeString(this.fuelCapacity);
        dest.writeString(this.speedLimit);
        dest.writeString(this.deviceType);
        dest.writeString(this.lastCommunication);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.signal);
        dest.writeString(this.address);
        dest.writeString(this.speed);
        dest.writeString(this.heading);
        dest.writeString(this.ignition);
        dest.writeString(this.fuelLevel);
        dest.writeString(this.satellite);
    }

    public DeviceBean() {
    }

    protected DeviceBean(Parcel in) {
        this.deviceID = in.readString();
        this.vehicleID = in.readString();
        this.licensePlate = in.readString();
        this.fuelCapacity = in.readString();
        this.speedLimit = in.readString();
        this.deviceType = in.readString();
        this.lastCommunication = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.signal = in.readString();
        this.address = in.readParcelable(String.class.getClassLoader());
        this.speed = in.readString();
        this.heading = in.readString();
        this.ignition = in.readString();
        this.fuelLevel = in.readString();
        this.satellite = in.readString();
    }

    public static final Creator<DeviceBean> CREATOR = new Creator<DeviceBean>() {
        @Override
        public DeviceBean createFromParcel(Parcel source) {
            return new DeviceBean(source);
        }

        @Override
        public DeviceBean[] newArray(int size) {
            return new DeviceBean[size];
        }
    };
}

