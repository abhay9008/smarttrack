package com.example.abhay.mapdemo.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.activity.Home;
import com.example.abhay.mapdemo.activity.LoginActivity;
import com.example.abhay.mapdemo.model.Location;
import com.example.abhay.mapdemo.networking.ApiClient;
import com.example.abhay.mapdemo.networking.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link GroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    private Location objLocation;
    private ProgressDialog dialog;
    private GoogleMap mMap;
    private Location mapLocation;

    public GroupFragment() {
        // Required empty public constructor
    }

    public static GroupFragment newInstance() {
        GroupFragment fragment = new GroupFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_group, container, false);
        objLocation = getArguments().getParcelable("location");
        setUpMap();
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Please wait");
        dialog.setMessage("Getting vehicle locations");
        return view;
    }

    private void setUpMap(){
        //map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.group_map)).getMap();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.group_map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getVehicleLocation();
    }

    private void getVehicleLocation() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        dialog.show();

        Call<Location> call = apiService.getLocations(objLocation.getmAccountId(), objLocation.getmUserId(), objLocation.getmPassword());
        call.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location>call, Response<Location> response) {
                dialog.dismiss();
                mapLocation = response.body();
                try {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (int i = 0; i < mapLocation.getDeviceBean().size(); i++) {
                        try {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(mapLocation.getDeviceBean().get(i).getLatitude()),
                                    Double.parseDouble(mapLocation.getDeviceBean().get(i).getLongitude()))).title(mapLocation.getDeviceBean().get(i).getLicensePlate()));
                            double lat = Double.parseDouble(mapLocation.getDeviceBean().get(i).getLatitude());
                            double lng = Double.parseDouble(mapLocation.getDeviceBean().get(i).getLongitude());
                            LatLng position = new LatLng(lat, lng);
                            builder.include(position);
                        } catch (NullPointerException ne) {
                            ne.printStackTrace();
                        }
                    }
                    LatLngBounds bounds = builder.build();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
                }catch (NullPointerException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Location>call, Throwable t) {
                // Log error here since request failed
                dialog.dismiss();
                Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG).show();
            }
        });
    }


}
