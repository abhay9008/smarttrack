package com.example.abhay.mapdemo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.model.Location;
import com.example.abhay.mapdemo.model.VehicleLocation;
import com.example.abhay.mapdemo.networking.ApiClient;
import com.example.abhay.mapdemo.networking.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.abhay.mapdemo.activity.LiveTracking.getTodaysDate;
import static com.example.abhay.mapdemo.activity.LiveTracking.getYesterdaysDate;

public class VehicleTracking extends AppCompatActivity implements OnMapReadyCallback {

    String mAccountId ="";
    String mUserId ="";
    String mPasssword ="";
    String mDeviceId ="";
    String mFromTime ="2017-10-24 09:50:00";
    String mToTime ="2017-10-24 10:20:00";
    private GoogleMap mMap;
    List<VehicleLocation> listOfLocation = new ArrayList<>();
    private ProgressDialog dialog;

    private CountDownTimer countDownTimer;
    private final long startTime =60 * 1000;
    private final long interval = 60 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_tracking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Live Tracking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                finish();
            }
        });
        dialog = new ProgressDialog(this);
        dialog.setTitle("Please wait");
        dialog.setMessage("Getting vehicle location");
        countDownTimer = new VehicleTracking.MyCountDownTimer(startTime, interval);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mAccountId = getIntent().getStringExtra("account_id");
        mUserId = getIntent().getStringExtra("user_id");
        mPasssword = getIntent().getStringExtra("password");
        mDeviceId = getIntent().getStringExtra("device_id");
        mFromTime = getYesterdaysDate();
        mToTime = getTodaysDate();
       /* mAccountId = "demo";
        mPasssword = "password";
        mDeviceId = "0351608083592050";*/
        getVehicleLocationData();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void getVehicleLocationData(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        dialog.show();

        Call<List<VehicleLocation>> call = apiService.getVehicleLocation(mAccountId, mUserId, mPasssword, mDeviceId, mFromTime, mToTime);
        call.enqueue(new Callback<List<VehicleLocation>>() {
            @Override
            public void onResponse(Call<List<VehicleLocation>>call, Response<List<VehicleLocation>> response) {
                listOfLocation = response.body();
                dialog.dismiss();
                try {
                    if (listOfLocation.size() > 0) {
                        ArrayList<LatLng> points = new ArrayList<>();
                        PolylineOptions lineOptions =  new PolylineOptions();

                        for (int i = 0; i < listOfLocation.size(); i++) {
                            if (listOfLocation.get(i).getLatitude() != null && !listOfLocation.get(i).getLatitude().isEmpty() && listOfLocation.get(i).getLongitude() != null && !listOfLocation.get(i).getLongitude().isEmpty()) {
                                double lat = Double.parseDouble(listOfLocation.get(i).getLatitude());
                                double lng = Double.parseDouble(listOfLocation.get(i).getLongitude());
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        lineOptions.addAll(points);
                        lineOptions.width(10);
                        lineOptions.color(Color.RED);

                        LatLngBounds.Builder bc = new LatLngBounds.Builder();

                        for (LatLng item : points) {
                            bc.include(item);
                        }

                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));

                        if (lineOptions != null) {
                            mMap.addPolyline(lineOptions);
                        } else {
                            Log.d("onPostExecute", "without Polylines drawn");
                        }

                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(listOfLocation.get(0).getLatitude()),
                                Double.parseDouble(listOfLocation.get(0).getLongitude()))));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(listOfLocation.get(listOfLocation.size() - 1).getLatitude()),
                                Double.parseDouble(listOfLocation.get(listOfLocation.size() - 1).getLongitude()))));

                    }
                    countDownTimer.start();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<VehicleLocation>>call, Throwable t) {
                // Log error here since request failed
                Log.d("onFailure", t.getMessage());
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        countDownTimer.cancel();
        super.onStop();
    }

    @Override
    protected void onPause() {
        countDownTimer.cancel();
        super.onPause();
    }

    @Override
    protected void onResume() {
        countDownTimer.start();
        super.onResume();
    }

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            getVehicleLocationData();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}
