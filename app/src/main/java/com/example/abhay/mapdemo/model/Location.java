
package com.example.abhay.mapdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location implements Parcelable {

    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("deviceDetails")
    @Expose
    private List<DeviceBean> deviceBean = null;

    private String mUserId;
    private String mAccountId;
    private String mPassword;

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public List<DeviceBean> getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(List<DeviceBean> deviceBean) {
        this.deviceBean = deviceBean;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmAccountId() {
        return mAccountId;
    }

    public void setmAccountId(String mAccountId) {
        this.mAccountId = mAccountId;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.loginStatus);
        dest.writeTypedList(this.deviceBean);
        dest.writeString(this.mUserId);
        dest.writeString(this.mAccountId);
        dest.writeString(this.mPassword);
    }

    public Location() {
    }

    protected Location(Parcel in) {
        this.loginStatus = in.readString();
        this.deviceBean = in.createTypedArrayList(DeviceBean.CREATOR);
        this.mUserId = in.readString();
        this.mAccountId = in.readString();
        this.mPassword = in.readString();
    }

    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel source) {
            return new Location(source);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}
