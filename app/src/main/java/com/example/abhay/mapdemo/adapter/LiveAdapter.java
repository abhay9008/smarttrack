package com.example.abhay.mapdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abhay.mapdemo.R;
import com.example.abhay.mapdemo.model.DeviceBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin on 16/10/17.
 */

public class LiveAdapter extends  RecyclerView.Adapter<LiveAdapter.LiveViewHolder>{

    private List<DeviceBean> listOfVehicles = new ArrayList<>();
    private Context cntxt;

    public LiveAdapter(Context cntxt, List<DeviceBean> listOfVehicles){
        this.cntxt = cntxt;
        this.listOfVehicles = listOfVehicles;
    }

    @Override
    public LiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_live_vehicle, parent, false);

        return new LiveViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LiveViewHolder holder, int position) {
        DeviceBean objLocation = listOfVehicles.get(position);
        holder.lisencePlate.setText(Html.fromHtml(getMessage( objLocation.getLicensePlate() )));
    }

    @Override
    public int getItemCount() {
        return listOfVehicles.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class LiveViewHolder extends RecyclerView.ViewHolder {
        public TextView lisencePlate;

        public LiveViewHolder(View view) {
            super(view);
            lisencePlate = (TextView) view.findViewById(R.id.vehicle_license_plate);
        }
    }

    private String getMessage(String message){
        try {
            if (message.isEmpty() || message.equals(null) || message.equals(" ")) {
                return "Not Available";
            } else
                return message;
        }catch (NullPointerException ne){
            ne.printStackTrace();
            return "Not Available";
        }
    }

}
