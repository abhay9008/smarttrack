package com.example.abhay.mapdemo.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

/**
 * Created by User on 2/9/2017.
 */

public class AppEditText extends TextInputEditText {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public AppEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public AppEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

        Typeface customFont = selectTypeface(context, textStyle);
        setTypeface(customFont);
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        /*
        * information about the TextView textStyle:
        *
        */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface("fonts/roboto-bold.ttf", context);

            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("fonts/roboto-italic.ttf", context);

            case Typeface.NORMAL: // regular
                return FontCache.getTypeface("fonts/roboto-normal.ttf", context);
            default:
                return FontCache.getTypeface("fonts/roboto-normal.ttf", context);
        }
    }

}
