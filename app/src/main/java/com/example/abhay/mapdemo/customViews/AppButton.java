package com.example.abhay.mapdemo.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by User on 4/24/2017.
 */

public class AppButton extends Button {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public AppButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

        Typeface customFont = selectTypeface(context, textStyle);
        setTypeface(customFont);
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        /*
        * information about the TextView textStyle:
        *
        */
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontCache.getTypeface("fonts/roboto-bold.ttf", context);

            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("fonts/roboto-italic.ttf", context);

            case Typeface.NORMAL: // regular
                return FontCache.getTypeface("fonts/roboto-normal.ttf", context);
            default:
                return FontCache.getTypeface("fonts/roboto-normal.ttf", context);
        }
    }

}
